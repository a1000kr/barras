import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
class HomePage extends StatefulWidget {

  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<charts.Series<Partida, String>> _seriesData = [];
  int variableInservible = 0;

  _generateData() {
    var ronda1 = [
      Partida(Jugador: "Jose", cartasAzules: 10, cartasVerdes: 0, cartasRosas: 0, cartasNegras: 0),
      Partida(Jugador: "Pedro", cartasAzules: 2, cartasVerdes: 0, cartasRosas: 0, cartasNegras: 0),
      Partida(Jugador: "Mario", cartasAzules: 1, cartasVerdes: 0, cartasRosas: 0, cartasNegras: 0)
    ];
    var ronda2 = [
      Partida(Jugador: "Jose", cartasAzules:  5, cartasVerdes: 5, cartasRosas: 0, cartasNegras: 0),
      Partida(Jugador: "Pedro", cartasAzules: 20, cartasVerdes: 0, cartasRosas: 0, cartasNegras: 0),
      Partida(Jugador: "Mario", cartasAzules: 2, cartasVerdes: 0, cartasRosas: 0, cartasNegras: 0)
    ];
    var ronda3 = [
      Partida(Jugador: "Jose", cartasAzules: 5, cartasVerdes: 5, cartasRosas: 0, cartasNegras: 0),
      Partida(Jugador: "Pedro", cartasAzules: 20, cartasVerdes: 0, cartasRosas: 0, cartasNegras: 0),
      Partida(Jugador: "Mario", cartasAzules: 3, cartasVerdes: 0, cartasRosas: 0, cartasNegras: 0)
    ];

    _seriesData.add(
      charts.Series(
        domainFn: (Partida pollution, _) => pollution.Jugador,
        measureFn: (Partida pollution, _) => pollution.cartasAzules,
        id: '1',
        data: ronda3,
        seriesCategory: '1',
        fillPatternFn: (_, __) => charts.FillPatternType.solid,
        fillColorFn: (Partida pollution, _) =>
            charts.ColorUtil.fromDartColor(Color(0xFF337CFF)),
      ), 
    );
    _seriesData.add(
      charts.Series(
        domainFn: (Partida pollution, _) => pollution.Jugador,
        measureFn: (Partida pollution, _) => pollution.cartasAzules,
        id: '2',
        data: ronda2,
        seriesCategory: '1',
        fillPatternFn: (_, __) => charts.FillPatternType.solid,
        fillColorFn: (Partida pollution, _) =>
           charts.ColorUtil.fromDartColor(Color(0xFF2D98DA)),
      ),
    );

    _seriesData.add(
      charts.Series(
        domainFn: (Partida pollution, _) => pollution.Jugador,
        measureFn: (Partida pollution, _) => pollution.cartasAzules,
        id: '3',
        data: ronda1,
        seriesCategory: '1',
        fillPatternFn: (_, __) => charts.FillPatternType.solid,
        fillColorFn: (Partida pollution, _) =>
           charts.ColorUtil.fromDartColor(Color(0xFF33A7FF)),
      ),
    );
//0xFF337CFF
//0xFF2D98DA
//0xFF33A7FF
    _seriesData.add(
      charts.Series(
        domainFn: (Partida pollution, _) => pollution.Jugador,
        measureFn: (Partida pollution, _) => pollution.cartasVerdes,
        id: '11',
        data: ronda3,
        seriesCategory: '2',
        fillPatternFn: (_, __) => charts.FillPatternType.solid,
        fillColorFn: (Partida pollution, _) =>
            charts.ColorUtil.fromDartColor(Color(0xFF33FF41)),
      ), 
    );
    _seriesData.add(
      charts.Series(
        domainFn: (Partida pollution, _) => pollution.Jugador,
        measureFn: (Partida pollution, _) => pollution.cartasVerdes,
        id: '12',
        data: ronda2,
        seriesCategory: '2',
        fillPatternFn: (_, __) => charts.FillPatternType.solid,
        fillColorFn: (Partida pollution, _) =>
           charts.ColorUtil.fromDartColor(Color(0xFF63D533)),
      ),
    );

    _seriesData.add(
      charts.Series(
        domainFn: (Partida pollution, _) => pollution.Jugador,
        measureFn: (Partida pollution, _) => pollution.cartasVerdes,
        id: '13',
        data: ronda1,
        seriesCategory: '2',
        fillPatternFn: (_, __) => charts.FillPatternType.solid,
        fillColorFn: (Partida pollution, _) =>
           charts.ColorUtil.fromDartColor(Color(0xFF449A20)),
      ),
    );

  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    //_seriesData =[];
    _generateData();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: DefaultTabController(
        length: 3,
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: Color(0xff1976d2),
            //backgroundColor: Color(0xff308e1c),
            bottom: TabBar(
              indicatorColor: Color(0xff9962D0),
              tabs: [
                Tab(
                  icon: Icon(FontAwesomeIcons.solidChartBar),
                ),
                Tab(icon: Icon(FontAwesomeIcons.chartPie)),
                Tab(icon: Icon(FontAwesomeIcons.chartLine)),
              ],
            ),
            title: Text('Flutter Charts'),
          ),
          body: TabBarView(
            children: [
              Padding(
                padding: EdgeInsets.all(8.0),
                child: Container(
                  child: Center(
                    child: Column(
                      children: <Widget>[
                        Text(
                            'Cartas jugadas en toda la partida',style: TextStyle(fontSize: 24.0,fontWeight: FontWeight.bold),),
                        Expanded(
                          child: charts.BarChart(
                            _seriesData,
                            animate: true,
                            barGroupingType: charts.BarGroupingType.groupedStacked,
                            //behaviors: [new charts.SeriesLegend()],
                            animationDuration: Duration(seconds: 0),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.all(8.0),
                child: Container(
                  child: Center(
                    child: Column(
                      children: <Widget>[
                        Text(
                            'Time spent on daily tasks',style: TextStyle(fontSize: 24.0,fontWeight: FontWeight.bold),),
                            SizedBox(height: 10.0,),
                      ],
                    ),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.all(8.0),
                child: Container(
                  child: Center(
                    child: Column(
                      children: <Widget>[
                        Text(
                            'Sales for the first 5 years',style: TextStyle(fontSize: 24.0,fontWeight: FontWeight.bold),),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class Partida {
  String Jugador;
  int cartasAzules;
  int cartasVerdes;
  int cartasRosas;
  int cartasNegras;
  Partida({
    required this.Jugador,
    required this.cartasAzules,
    required this.cartasVerdes,
    required this.cartasRosas,
    required this.cartasNegras,
  });

}

